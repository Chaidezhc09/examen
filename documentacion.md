### ARANDA CASTILLO MIGUEL ÁNGEL 8~A
### BRITO GARCÍA ÁNGEL EMILIO 8~A
### CHAIDEZ TORRECILLAS LUIS ENRIQUE 8~A
### GARCÍA ALMAZÁN ASHLEY MICHELLE 8~A
### PEÑA LEÓN JONATHAN DANIEL 8~A
### ZAMORANO PALMA JOSE HECTOR 8~B


# Práctica de Desarrollo Web Profesional

## Instrucciones

Lee detalladamente el problema y realiza lo que se solicita de la forma correcta.

La empresa Parra’s Dev requiere una aplicación para llevar control de las tareas pendientes que tiene su equipo de auditores de las aplicaciones móviles que forman parte de su inventario de servicios y productos. Por este motivo, le pide a sus desarrolladores una aplicación ToDo para que Parra’s Dev pueda supervisar el avance de sus auditorías, con el objetivo de mejorar la calidad de sus servicios y productos.

Parra’s Dev espera de su equipo de desarrollo una aplicación ToDo que cumpla con las técnicas necesarias para considerarse como un desarrollo Web Profesional. La información oportuna y confiable le permite a Parra’s Dev tomar las mejores decisiones para la evolución de sus servicios y productos. Motivo por el cual Parra’s Dev confía en que sus desarrolladores hacen códigos fuentes con calidad.

## Pasos para realizar la práctica

1. **Crear un repositorio para el equipo en GitLab**.
2. Descargar el complemento **Live Share** en Visual Studio Code para que todo el equipo pueda editar libremente el código y se vean los cambios.
3. Abrir la terminal de comandos y desde ahí **clonar el repositorio de GitLab**.
4. Instalar las dependencias en la carpeta con los comandos:
    ```sh
    pip install virtualenv
    virtualenv -p python3 venv
    ```
5. Dentro de la carpeta “venv” ir a la dirección `bin` y `activate` para levantar el entorno virtual del proyecto. Comando:
    ```sh
    source venv/bin/activate
    ```
6. Instalar el framework de Django en el cmd con:
    ```sh
    pip install django psycopg2-binary
    ```
7. Una vez instalado Django se necesitará crear el proyecto de Django con el comando:
    ```sh
    django-admin startproject myproject
    ```
   Acceder al proyecto en la terminal con el comando:
    ```sh
    cd myproject
    ```
8. Dentro de la carpeta del proyecto, crear una aplicación con el comando:
    ```sh
    python manage.py startapp prueba
    ```
9. Correr el servidor con el comando:
    ```sh
    python manage.py runserver
    ```
   En el navegador, poner la dirección `localhost:8000` para visualizar la interfaz de Django.
10. En el archivo `settings.py`, configurar el motor de base de datos a Postgresql (array `DATABASES`).
11. Dar de alta la aplicación (LOCAL_APPS) y agregar el nombre de la app `prueba` igualmente en `settings.py`.
12. Acceder a la terminal de postgres con el comando:
    ```sh
    sudo su postgres
    psql
    ```
13. Crear una base de datos llamada “Examen” con un usuario de Postgresql. Por terminal se puede hacer con el comando:
    ```sql
    CREATE DATABASE Examen WITH OWNER postgres;
    ```
14. Migrar los modelos con los comandos:
    ```sh
    python3 manage.py makemigrations
    python3 manage.py migrate
    ```
15. Crear un super usuario con el comando:
    ```sh
    python manage.py createsuperuser
    ```
16. Abrir la carpeta del proyecto en Visual Studio Code para empezar a añadir las rutas y demás configuraciones.
17. Realizar un:
    ```sh
    pip freeze > requirements.txt
    ```
    para tener las dependencias descargadas para la realización de este proyecto.
18. Ahora el proyecto está listo para darle funcionalidad y diseño.


## Pasos adicionales de creacion del modelado

**Definir los modelos**  
Definir los modelos necesarios para la aplicación de tareas. Un modelo típico podría ser `Task` que incluye campos como título, descripción, fecha de creación, fecha de vencimiento, estado, prioridad, etc.

**Registrar el modelo en admin**  
    Registrar los modelos en el archivo `admin.py` para poder gestionarlos a través del sitio de administración de Django.

**Crear formularios**  
Crear formularios para gestionar las tareas. Puedes usar `ModelForm` de Django para simplificar este proceso.

**Crear vistas**  
Crear las vistas necesarias para listar, crear, editar y eliminar tareas.

**Configurar URLs**  
Configurar las URLs para las vistas.

**Crear templates**  
Crear los templates para listar, crear, editar y eliminar tareas.

**Aplicar migraciones**  
Asegurarse de que tus migraciones estén actualizadas.