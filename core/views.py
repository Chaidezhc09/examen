from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy

from core import models
from core import forms 

# Create your views here.


##TAREAS CRUD

### CREATE
class NewTarea(generic.CreateView):
    template_name = "core/create_Tarea.html"
    model = models.Tarea
    form_class = forms.CreateTareaForm
    success_url = reverse_lazy("core:list_Tarea")

## List
class ListTarea(generic.View):
    template_name = "core/list_Tarea.html"
    context = {}

    def get(self, request):
        self.context = {
            "Tarea": models.Tarea.objects.filter(status=True)
        }
        return render(request, self.template_name, self.context)
    
## Detail
class DetailTarea(generic.View):
    template_name = "core/detail_Tarea.html"
    context = {}

    def get(self, request, pk):
        self.context = {
            "Tarea": models.Tarea.objects.get(pk=pk)
        }
        return render(request, self.template_name, self.context)

### UPDATE
class UpdateTarea(generic.UpdateView):
    template_name = "core/update_Tarea.html"
    model = models.Tarea
    form_class = forms.UpdateTareaForm
    success_url = reverse_lazy("core:list_Tarea")


### DELETE
class DeleteTarea(generic.DeleteView):
    template_name = "core/delete_Tarea.html"
    model = models.Tarea
    success_url = reverse_lazy("core:list_Tarea")


##PROYECTOS CRUD


### CREATE
class NewProyecto(generic.CreateView):
    template_name = "core/create_Proyecto.html"
    model = models.Proyecto
    form_class = forms.CreateProyectoForm
    success_url = reverse_lazy("core:list_Proyecto")

### LIST
class ListProyecto(generic.View):
    template_name = "core/list_Proyecto.html"
    context = {}

    def get(self, request):
        self.context = {
            "proyecto": models.Proyecto.objects.all()
        }
        return render(request, self.template_name, self.context)
    
### DETAIL
class DetailProyecto(generic.View):
    template_name = "core/detail_Proyecto.html"
    context = {}

    def get(self, request, pk):
        self.context = {
            "proyecto": models.Proyecto.objects.get(pk=pk)
        }
        return render(request, self.template_name, self.context)

### UPDATE
class UpdateProyecto(generic.UpdateView):
    template_name = "core/update_Proyecto.html"
    model = models.Proyecto
    form_class = forms.UpdateProyectoForm
    success_url = reverse_lazy("core:list_Proyecto")

### DELETE
class DeleteProyecto(generic.DeleteView):
    template_name = "core/delete_Proyecto.html"
    model = models.Proyecto
    success_url = reverse_lazy("core:list_Proyecto")