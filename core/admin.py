from django.contrib import admin

# Register your models here.
from core import models

@admin.register(models.Tarea)
class Tarea(admin.ModelAdmin):
    list_display = ["Tareanombre", "Usuario", "timestamp", "fecha_final", "dias_duracion", "status"]
