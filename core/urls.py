from django.urls import path

from core import views

app_name = "core"


urlpatterns = [

    ##URLS TAREAS
        path('', views.ListTarea.as_view(), name="List_Tarea"),
    path('core/list/Tarea/', views.ListTarea.as_view(), name="list_Tarea"),
    path('core/new/Tarea/', views.NewTarea.as_view(), name="create_Tarea"),
    path('core/detail/Tarea/<int:pk>/', views.DetailTarea.as_view(), name="detail_Tarea"),
    path('core/update/Tarea/<int:pk>/', views.UpdateTarea.as_view(), name="update_Tarea"),
    path('core/delete/Tarea/<int:pk>/', views.DeleteTarea.as_view(), name="delete_Tarea"),


    ##URLS PROYECTOS
    path('core/list/proyecto/', views.ListProyecto.as_view(), name="list_Proyecto"),
    path('core/new/proyecto/', views.NewProyecto.as_view(), name="create_Proyecto"),
    path('core/detail/proyecto/<int:pk>/', views.DetailProyecto.as_view(), name="detail_Proyecto"),
    path('core/update/proyecto/<int:pk>/', views.UpdateProyecto.as_view(), name="update_Proyecto"),
    path('core/delete/proyecto/<int:pk>/', views.DeleteProyecto.as_view(), name="delete_Proyecto"),


]