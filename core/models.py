from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from datetime import timedelta

PRIORIDAD = (
    ("URGENTE", "URG"),
    ("PENDIENTE", "NEC"),
    ("ASIGNADO", "ASIG"),
)

ESTADO = (
    ("NOT STARTED", "NS"),
    ("DOING", "DG"),
    ("DONE", "DN"),
)

class Proyecto(models.Model):
    nombre = models.CharField(max_length=64, default="PROYECTO 1")
    descripcion = models.CharField(max_length=128, default="Descripción del proyecto")
    fecha_inicio = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.nombre

class Tarea(models.Model):
    Tareanombre = models.CharField(max_length=64, default="TAREA 1")
    descripcion = models.CharField(max_length=128, default="Descripción de la tarea")
    Usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    Proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE, related_name='tareas')
    timestamp = models.DateField(auto_now_add=True)
    dias_duracion = models.IntegerField(default=7)
    fecha_final = models.DateField(auto_now_add=False, auto_now=False, blank=True, null=True)
    progreso = models.FloatField(default=0.0)
    prioridad = models.CharField(max_length=16, choices=PRIORIDAD)
    estado = models.CharField(max_length=16, choices=ESTADO)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.Tareanombre


