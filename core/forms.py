from django import forms
from .models import Tarea, Proyecto

class CreateTareaForm(forms.ModelForm):
    class Meta:
        model = Tarea
        fields = [
            "Tareanombre",
            "descripcion",
            "Usuario",
            "Proyecto",
            "dias_duracion",
            "progreso",
            "prioridad",
            "estado",
            "status"
        ]
        widgets = {
            "Tareanombre": forms.TextInput(attrs={"class":"form-control", "placeholder":"Nombre de la Tarea"}),
            "descripcion": forms.Textarea(attrs={"class":"form-control", "placeholder":"Descripción"}),
            "Usuario": forms.Select(attrs={"class":"form-control"}),
            "Proyecto": forms.Select(attrs={"class":"form-control"}),
            "dias_duracion": forms.NumberInput(attrs={"class":"form-control"}),
            "progreso": forms.NumberInput(attrs={"class":"form-control"}),
            "prioridad": forms.Select(attrs={"class":"form-control"}),
            "estado": forms.Select(attrs={"class":"form-control"}),
            "status": forms.CheckboxInput(attrs={"class":"form-check-input"})
        }

class UpdateTareaForm(forms.ModelForm):
    class Meta:
        model = Tarea
        fields = [
            "Tareanombre",
            "descripcion",
            "Usuario",
            "Proyecto",
            "dias_duracion",
            "progreso",
            "prioridad",
            "estado",
            "status"
        ]
        widgets = {
            "Tareanombre": forms.TextInput(attrs={"class":"form-control", "placeholder":"Nombre de la Tarea"}),
            "descripcion": forms.Textarea(attrs={"class":"form-control", "placeholder":"Descripción"}),
            "Usuario": forms.Select(attrs={"class":"form-control"}),
            "Proyecto": forms.Select(attrs={"class":"form-control"}),
            "dias_duracion": forms.NumberInput(attrs={"class":"form-control"}),
            "progreso": forms.NumberInput(attrs={"class":"form-control"}),
            "prioridad": forms.Select(attrs={"class":"form-control"}),
            "estado": forms.Select(attrs={"class":"form-control"}),
            "status": forms.CheckboxInput(attrs={"class":"form-check-input"})
        }

class CreateProyectoForm(forms.ModelForm):
    class Meta:
        model = Proyecto
        fields = [
            "nombre",
            "descripcion"

        ]
        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Nombre del proyecto"}),
            "descripcion": forms.Textarea(attrs={"rows": 3, "type": "text", "class": "form-control", "placeholder": "Descripción del proyecto"})
        }

class UpdateProyectoForm(forms.ModelForm):
    class Meta:
        model = Proyecto
        fields = [
            "nombre",
            "descripcion"
        ]
        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Nombre del proyecto"}),
            "descripcion": forms.Textarea(attrs={"rows": 3, "type": "text", "class": "form-control", "placeholder": "Descripción del proyecto"})
        }